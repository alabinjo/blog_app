class CommentsController < ApplicationController
  
  def index 
  end 
  
  def new
    @article = Article.find(params[:id])
    @commment = @article.comment.new 
  end 
  
  def create
    @article = Article.find(params[:article_id])
    @comment = @article.comments.new(comment_params)
    if @comment.save
      flash[:success] = "Comment has been created"
      redirect_to article_path(@article)
    else 
      flash[:error] = "Comment failed"
      render :new
    end 
  end 
  
  def show
    article = Article.find(params[:article_id])
    @comment = article.comments.find(params[:id])
  end 
  
  def edit 
    article = Article.find(params[:article_id])
    @comment = article.comments.find(params[:id])
  end 
  
  def update
    article = Article.find(params[:article_id])
    @comment = article.comments.find(params[:id])
    if @comment.update(comment_params)
      flash[:success] = "Comment updated successfully"
      redirect_to article_comment_path(@comment.article, @comment)
    else 
      flash.now[:error] = "Comment Error"
      render :edit 
    end 
  end 
  
  def destroy
    article = Article.find(params[:article_id])
    @comment = article.comments.find(params[:id])
    @comment.destroy 
    redirect_to article_path(@comment.article)
  end 
  
  def delete 
  end 
  
  def comment_params
    params.require(:comment).permit(:name, :body, :id, :article_id)
  end 
  
end
