class ArticlesController < ApplicationController
  
  def index 
    @articles = Article.all
  end 
  
  def new 
    @article = Article.new 
  end 
  
  def create
    @article = Article.new(article_params)
    if @article.save
      flash[:success] = "Your article has been created"
      redirect_to articles_path 
    else 
      flash.now[:error] = "There is a problem with your form"
      render :new
    end 
  end 
  
  def show 
    @article = Article.find(params[:id])
  end 
  
  def edit 
    @article = Article.find(params[:id])
  end 
  
  def update 
    @article = Article.find(params[:id])
    if @article.update(article_params)
      flash[:success] = "Updated successfully"
      redirect_to articles_path 
    else 
      flash[:error] = "Update failed"
      render :edit 
    end 
  end 
  
  def destroy 
    @article = Article.find(params[:id])
    @article.destroy
    redirect_to articles_path
  end 
  
  def delete 
    
  end 
  
  private 
  
  def article_params 
    params.require(:article).permit(:first_name, :last_name, :title, :text, :id)
  end 
  
end
